require 'sinatra/base'
require 'sinatra/cross_origin'
require 'net/http'
require 'timers'

set :bind, '0.0.0.0'
  configure do
    enable :cross_origin
  end
  before do
    response.headers['Access-Control-Allow-Origin'] = '*'
  end

class Api < Sinatra::Base
    @proxy_addr = '178.32.196.230'
    @proxy_port = 8080
    @endpoint = 'https://m.betvictor.com/bv_in_play/v2/en-gb/1/in_play/overview.json'
    @data = ''

    def initialize
        now_and_every_five_seconds = timers.now_and_every(5) {
            fetch()
        }
        loop { timers.wait }
    end

    def fetch()

    end


  # Routes
  get '/' do
    uri = URI.parse(endpoint)
    proxy = Net::HTTP::Proxy(proxy_addr, proxy_port)
    req = Net::HTTP::Get.new(uri.path)
    result = proxy.start(uri.host,uri.port, :use_ssl => true) do |http|
      http.request(req)
    end

    p result.body
  end


  options "*" do
    response.headers["Allow"] = "GET, PUT, POST, DELETE, OPTIONS"
    response.headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type, Accept, X-User-Email, X-Auth-Token"
    response.headers["Access-Control-Allow-Origin"] = "*"
    200
  end
end


run Api.run!





#178.32.196.230	8080
#81.199.32.90	40045
#185.112.249.194	3128
#51.140.249.129	8080