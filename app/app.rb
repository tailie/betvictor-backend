# frozen_string_literal: true
require "date"
require "json"
require 'sinatra/base'
require 'sinatra/cross_origin'
require 'sinatra/async'
require 'thin'
require 'net/http'
require 'timers'
require 'eventmachine'


require "./app/services/data_service"
require "./app/models/resource"
require "./app/models/event"
require "./app/models/sport"

require "./app/controllers/application_controller"
