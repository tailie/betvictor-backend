require 'sinatra/async'

module Api
  class App < Sinatra::Base
    register Sinatra::Async

    @@sports = []
    def self.sports
      @@sports
    end
    # use Raven::Rack

    # helpers Sinatra::CustomLogger
    # helpers Sinatra::Param

    configure :development, :production do
      enable :cross_origin

      # logger = Timber::Logger.new(STDOUT)

      # logger.level = Logger::DEBUG if development?
      # set :logger, logger

      # Timber::Integrations::Rack.middlewares.each do |middleware|
      #   use middleware
      # end
    end

    def fetch
      # set :bind, '0.0.0.0'
      data_service = DataService.new
      data_service.fetch(method(:handle_success))
    end
    def handle_success(data = [])
      @@sports = []
      data.each do |result|
        # new_sport = Sport.new({id: result["id"], name: result["name"]})
        # p result.to_json
        # "#{ROOT_DIR}/#{project}/App.config"
        events = result["comp"].map { |c| c["events"].map { |e| e["url"] = "/sports/#{result["id"]}/events/#{e["id"]}"; e["comp"] = c["desc"]; e }; c["events"] }.flatten()
        @@sports << Sport.new({id: result["id"], events: events, name: result["desc"]})
      end
      body @@sports.to_json
    end
    before do
      # enable CORS
      response.headers['Access-Control-Allow-Origin'] = '*'

      # we almost always want a JSON output
      content_type :json
    end

    #
    # List all sports
    #
    aget "/" do
      fetch()
    end

    get "/sports" do
      body @@sports.to_json
    end
    #
    # List all events for a given sport
    #
    get "/sports/:sport_id" do
        p @@sports.find {|sport| sport.id == Integer(params["sport_id"])}.to_json
    end

    #
    # List all outcomes for a given event
    #
    get "/sports/:sport_id/events/:event_id" do
      sport = @@sports.find {|sport| sport.id == Integer(params["sport_id"])}

      events = []
      sport.events.each do |result|
        events << Event.new(result)
      end

      event = events.find {|event| event.id == Integer(params["event_id"])}
      p event.to_json
    end

    #
    # Error Handling
    #
    not_found do
      { status: 404, message: "Not Found" }.to_json
    end
  end
end