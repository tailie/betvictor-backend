# frozen_string_literal: true
module Api
  class Sport < Resource
    attr_accessor :id, :name, :events

    def to_json(_options = nil)
          h = { id: id, name: name, events: events}
          h.to_json
    end
  end
end