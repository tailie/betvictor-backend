# frozen_string_literal: true
module Api
  class Event < Resource
    attr_accessor :id, :desc, :url, :comp, :results, :markets, :scoreboard, :oppADesc, :oppBDesc, :time

    def to_json(_options = nil)
      h = { id: id, name: desc, url: url, comp: comp, results: results, markets: markets, scoreboard: scoreboard, oppADesc: oppADesc, oppBDesc: oppBDesc, time: time}
      h.to_json
    end
  end
end