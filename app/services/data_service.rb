# frozen_string_literal: true
module Api
  class DataService
    ENDPOINT = "https://m.betvictor.com/bv_in_play/v2/en-gb/1/in_play/overview.json"
    PROXY_ADDR = '178.128.168.122'
    PROXY_PORT = 8080

    attr_reader :agent

    def initialize
    end

    # Fetch all data.
    def fetch(callback)
        Thread.new do
            uri = URI.parse(ENDPOINT)
            proxy = Net::HTTP::Proxy(PROXY_ADDR, PROXY_PORT)
            req = Net::HTTP::Get.new(uri.path)
            result = proxy.start(uri.host,uri.port, :use_ssl => true) do |http|
              response = http.request(req)
            end
            # p JSON.parse(result.body)['sports']
            callback.call(JSON.parse(result.body)['sports'])

          # Do normal Net::HTTP stuff here.
          # uri = URI.parse("http://google.com/")
          # http = Net::HTTP.new(uri.host, uri.port)
          # response = http.request(Net::HTTP::Get.new(uri.request_uri))
        end.join
    end
  end
end