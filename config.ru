# frozen_string_literal: true
$LOAD_PATH.push File.expand_path("./app", __FILE__)
require "./app/app"

at_exit do
  puts 'Damn!'
  exit true
end

run Api::App
